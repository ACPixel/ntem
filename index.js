var express = require('express')
  , app = express()
  , server = require('http').createServer(app)
  , util = require('util')
  , path = require('path')
  , port = 3000
  , fs = require("fs")
  , api = require('./api');

app.use(require('node-sass-middleware')({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true,
  sourceMap: true
}));
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');

app.get('/', function (req, res) {
  res.render('index', { title: "Index"});
});

app.use('/api', api);

server.listen(port);
